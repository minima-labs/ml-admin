<?php

/**
 * Entity sub-class used for bundle entities.
 */
class MLEntityType extends Entity {
  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
